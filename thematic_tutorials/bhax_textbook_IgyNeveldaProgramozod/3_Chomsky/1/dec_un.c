#include <stdio.h>
int 
main() 
{
	int x;
	printf("Adjon meg egy decimális számot!\n");
	scanf ("%d", &x);
	for (int i = 0; i<x; i++)			//ciklus, ami a decimális számszor ismétlődik 
	{
		if ((i%5==0) && (i!=0)) printf (" "); 	//5-ös csoportosítás (az átláthtóság érdekében)
		printf ("|");				//unáris számjegyek kiírása
	}
	printf("\n");
	return 0;
}

