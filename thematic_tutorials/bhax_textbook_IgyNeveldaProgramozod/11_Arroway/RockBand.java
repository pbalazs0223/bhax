public class RockBand {

        private String name;
        private Integer dateOfFoundation;
        private String nameOfMember;

    public RockBand(String name, Integer dateOfFoundation, String nameOfMember) {
        this.name = name;
        this.dateOfFoundation = dateOfFoundation;
        this.nameOfMember = nameOfMember;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RockBand rockBand = (RockBand) o;

        if (name != null ? !name.equals(rockBand.name) : rockBand.name != null) return false;
        if (dateOfFoundation != null ? !dateOfFoundation.equals(rockBand.dateOfFoundation) : rockBand.dateOfFoundation != null)
            return false;
        return nameOfMember != null ? nameOfMember.equals(rockBand.nameOfMember) : rockBand.nameOfMember == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (dateOfFoundation != null ? dateOfFoundation.hashCode() : 0);
        result = 31 * result + (nameOfMember != null ? nameOfMember.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RockBand{" +
                "name='" + name + '\'' +
                ", dateOfFoundation=" + dateOfFoundation +
                ", nameOfMember='" + nameOfMember + '\'' +
                '}';
    }

    public static void main(String[] args) {

        RockBand ironMaiden = new RockBand("Iron Maiden",1975, "Bruce, Steve, Dave, Adrian, Nicko, Janick");
        RockBand acdc = new RockBand("AC/DC", 1973, "Angus, Brian, Stevie, Cliff, Phil");
        RockBand ironMaidnem = new RockBand("Iron Maiden",1975, "Bruce, Steve, Dave, Adrian, Nicko, Janick");

        System.out.println(ironMaiden.equals(ironMaidnem));

        System.out.println(ironMaiden.toString());
    }

}
