package Chomsky_OOE;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class OrderOfEverything {
    private static <T extends Comparable<T>> List<T> createOrderedList(Collection<T> input) {
        return input.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        Collection<Integer> input = Set.of(4, 2, 3);
        Collection<Integer> input2 = List.of(78, 21, 0);
        List<Integer> actualOutput = createOrderedList(input);
        List<Integer> actualOutput2 = createOrderedList(input2);
        System.out.println(actualOutput);
        System.out.println(actualOutput2);
    }
}
