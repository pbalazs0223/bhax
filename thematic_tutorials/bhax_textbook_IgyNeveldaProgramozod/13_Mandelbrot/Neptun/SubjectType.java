package Neptun;

public enum SubjectType {
    OBLIGATORY,
    OPTIONAL,
    ELECTIVE
}
