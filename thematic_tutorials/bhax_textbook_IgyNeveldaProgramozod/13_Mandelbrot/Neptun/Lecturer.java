package Neptun;

import java.util.Set;

public class Lecturer {
    private String name;
    private Set<Course> courses;

    public String getName() {
        return name;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void addCourses(Course course) {
        this.courses.add(course);
    }

    public void removeCourses(Course course) {
        this.courses.remove(course);
    }
}
