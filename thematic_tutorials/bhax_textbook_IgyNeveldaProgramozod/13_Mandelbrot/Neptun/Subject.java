package Neptun;

public class Subject {
    private String name;
    private String subjectID;
    private Integer credit;
    private Lecturer responsibleLecturer;
    private SubjectType subjectType;

    public String getName() {
        return name;
    }

    public String getSubjectID() {
        return subjectID;
    }

    public Integer getCredit() {
        return credit;
    }

    public Lecturer getResponsibleLecturer() {
        return responsibleLecturer;
    }

    public SubjectType getSubjectType() {
        return subjectType;
    }
}
