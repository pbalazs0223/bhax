package Neptun;

import java.util.Date;
import java.util.Set;

public class Course {

    private String courseID;
    private Date date;
    private CourseType courseType;
    private Lecturer lecturer;
    private Subject subject;
    private Set<Student> students;
    private Integer capacity;

    public String getCourseID() {
        return courseID;
    }

    public Date getDate() {
        return date;
    }

    public CourseType getCourseType() {
        return courseType;
    }

    public Lecturer getLecturer() {
        return lecturer;
    }

    public Subject getSubject() {
        return subject;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void addStudents(Student student) {
        this.students.add(student);
    }

    public void removeStudents(Student student) {
        this.students.remove(student);
    }
}
