package Neptun;

import java.util.Set;

public class Student {

    private String name;
    private String studentID;
    private Set<Course> courses;

    public String getName() {
        return name;
    }

    public String getStudentID() {
        return studentID;
    }

    public Set<Course> getCourses() {
        return courses;
    }

    public void addCourses(Course course) {
        this.courses.add(course);
    }

    public void removeCourses(Course course) {
        this.courses.remove(course);
    }
}
