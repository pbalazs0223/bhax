package Neptun;

public enum CourseType {
    LABORATORY,
    PRACTICAL,
    THEORETICAL,
    EXAM_COURSE
}
