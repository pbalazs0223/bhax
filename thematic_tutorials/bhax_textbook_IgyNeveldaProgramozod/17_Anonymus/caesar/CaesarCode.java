package caesar;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Scanner;

public class CaesarCode {

    static Scanner scanner = new Scanner(System.in);
    static OutputStream outputStream;
    static String input = "";
    static int offset = 0;
    static final int ASCII_UPPERCASE_START = 65;
    static final int ASCII_LOWERCASE_START = 97;

    static void writeToFile(String input) throws IOException{
        shiftedString(input);
        outputStream.write("\n".getBytes());
    }

    static void shiftedString(String text) throws IOException {
        for (Character character : text.toCharArray()){
            if (Character.isUpperCase(character)){
                outputStream.write((char)(ASCII_UPPERCASE_START+(character-ASCII_UPPERCASE_START+offset)));
            } else if (Character.isLowerCase(character)){
                outputStream.write((char)(ASCII_LOWERCASE_START+(character-ASCII_LOWERCASE_START+offset)));
            } else {
                outputStream.write(character);
            }
        }
    }

    public static void main(String[] args) throws IOException{
        outputStream = args[0] ==null ? System.out : new FileOutputStream(args[0]);
        offset = Integer.parseInt(args[1]);
        System.out.println("Text ('q' to quit): ");
        while (true){
            input = scanner.nextLine();
            if (input.equals("q")){
                break;
            } else {
                writeToFile(input);
            }
        }
    }
}
