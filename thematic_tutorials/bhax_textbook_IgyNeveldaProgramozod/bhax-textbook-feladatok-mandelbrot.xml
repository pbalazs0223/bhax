<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Mandelbrot!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>
    <section xml:id="bhax-textbook-feladatok-mandelbrot.Mandelbrot">
        <title>A Mandelbrot halmaz</title>
        <para>
            Írj olyan C programot, amely kiszámolja a Mandelbrot halmazt!     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/A-UEHNhyKUw">https://youtu.be/A-UEHNhyKUw</link>
        </para>
        <para>
            Megoldás forrása:                
                            <link xlink:href="5_Mandelbrot/1/mandelbrot.c++"><filename>mandelbrot.c++</filename>
            </link>            
        </para>     
        <para>
            A Mandelbrot halmazt 1980-ban találta meg Benoit Mandelbrot a 
            komplex számsíkon. Komplex számok azok a számok, amelyek körében 
            válaszolni lehet az olyan egyébként értelmezhetetlen kérdésekre, 
            hogy melyik az a két szám, amelyet összeszorozva -9-et kapunk, 
            mert ez a szám például a 3i komplex szám.
        </para>
        <para>             
            A Mandelbrot halmazt úgy láthatjuk meg, hogy a sík origója középpontú 4 
            oldalhosszúságú négyzetbe lefektetünk egy, mondjuk 800x800-as 
            rácsot és kiszámoljuk, hogy a rács pontjai mely komplex számoknak 
            felelnek meg. A rács minden pontját megvizsgáljuk a 
            z<subscript>n+1</subscript>=z<subscript>n</subscript>
            <superscript>2</superscript>+c, 
            (0&lt;=n) képlet alapján úgy, hogy a c az éppen vizsgált rácspont. 
            A z0 az origó. Alkalmazva a képletet a 
        </para>
        <itemizedlist>
            <listitem>
                <para>
                    z<subscript>0</subscript> = 0
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>1</subscript> = 0<superscript>2</superscript>+c = c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>2</subscript> = c<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>3</subscript> = (c<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    z<subscript>4</subscript> = ((c<superscript>2</superscript>+c)<superscript>2</superscript>+c)<superscript>2</superscript>+c
                </para>
            </listitem>
            <listitem>
                <para>
                    ... s így tovább.
                </para>
            </listitem>
        </itemizedlist>
        <para>
            Azaz kiindulunk az origóból (z<subscript>0</subscript>) 
            és elugrunk a rács első pontjába a z<subscript>1</subscript> = c-be, 
            aztán a c-től függően a további z-kbe. Ha ez az utazás kivezet a 
            2 sugarú körből, akkor azt mondjuk, hogy az a vizsgált rácspont 
            nem a Mandelbrot halmaz eleme. Nyilván nem tudunk végtelen sok 
            z-t megvizsgálni, ezért csak véges sok z elemet nézünk meg minden 
            rácsponthoz. Ha eközben nem lép ki a körből, akkor feketére 
            színezzük, hogy az a c rácspont a halmaz része. (Színes meg úgy 
            lesz a kép, hogy változatosan színezzük, például minél későbbi 
            z-nél lép ki a körből, annál sötétebbre). 
        </para>  
        <para>
        A programkód:
        </para>
        <programlisting language="c++"> <![CDATA[
#include <iostream>
#include "png++/png.hpp"
#include <sys/times.h>

#define MERET 600
#define ITER_HAT 32000

void
mandel (int kepadat[MERET][MERET]) {

    // Mérünk időt (PP 64)
    clock_t delta = clock ();
    // Mérünk időt (PP 66)
    struct tms tmsbuf1, tmsbuf2;
    times (&tmsbuf1);

    // számítás adatai
    float a = -2.0, b = .7, c = -1.35, d = 1.35;
    int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;

    // a számítás
    float dx = (b - a) / szelesseg;
    float dy = (d - c) / magassag;
    float reC, imC, reZ, imZ, ujreZ, ujimZ;
    // Hány iterációt csináltunk?
    int iteracio = 0;
    // Végigzongorázzuk a szélesség x magasság rácsot:
    for (int j = 0; j < magassag; ++j)
    {
        //sor = j;
        for (int k = 0; k < szelesseg; ++k)
        {
            // c = (reC, imC) a rács csomópontjainak
            // megfelelő komplex szám
            reC = a + k * dx;
            imC = d - j * dy;
            // z_0 = 0 = (reZ, imZ)
            reZ = 0;
            imZ = 0;
            iteracio = 0;
            // z_{n+1} = z_n * z_n + c iterációk
            // számítása, amíg |z_n| < 2 vagy még
            // nem értük el a 255 iterációt, ha
            // viszont elértük, akkor úgy vesszük,
            // hogy a kiinduláci c komplex számra
            // az iteráció konvergens, azaz a c a
            // Mandelbrot halmaz eleme
            while (reZ * reZ + imZ * imZ < 4 && iteracio < iteraciosHatar)
            {
                // z_{n+1} = z_n * z_n + c
                ujreZ = reZ * reZ - imZ * imZ + reC;
                ujimZ = 2 * reZ * imZ + imC;
                reZ = ujreZ;
                imZ = ujimZ;

                ++iteracio;

            }

            kepadat[j][k] = iteracio;
        }
    }

    times (&tmsbuf2);
    std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
              + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

    delta = clock () - delta;
    std::cout << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;

}

int
main (int argc, char *argv[])
{

    if (argc != 2)
    {
        std::cout << "Hasznalat: ./mandelpng fajlnev";
        return -1;
    }

    int kepadat[MERET][MERET];

    mandel(kepadat);

    png::image < png::rgb_pixel > kep (MERET, MERET);

    for (int j = 0; j < MERET; ++j)
    {
        //sor = j;
        for (int k = 0; k < MERET; ++k)
        {
            kep.set_pixel (k, j,
                           png::rgb_pixel (255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT,
                                           255 -
                                           (255 * kepadat[j][k]) / ITER_HAT));
        }
    }

    kep.write (argv[1]);
    std::cout << argv[1] << " mentve" << std::endl;

}      
    ]]>
        </programlisting>
        <figure>
            <title>A Mandelbrot halmaz</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/1/out.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>      
            <para>
                A program fordítása és futtatása:
            </para>
            <screen><![CDATA[
$ g++ mandelbrot.c++ -lpng -o mandelbrot
$ ./mandelbrot out.png
1449
14.5002 sec
out.png mentve
]]></screen> 
    </section>
           
    <section>
        <title>A Mandelbrot halmaz a <filename>std::complex</filename> osztállyal</title>
        <para>
            Írj olyan C++ programot, amely kiszámolja a Mandelbrot halmazt!                     
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/v6PY622uMBI">https://youtu.be/v6PY622uMBI</link>
        </para>
        <para>
            Megoldás forrása:<link xlink:href="5_Mandelbrot/2/mandelcomp.cpp"><filename>mandelcomp.cpp</filename></link>            
        </para>
        <para>        
            A <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmaz</link> pontban vázolt
            ismert algoritmust valósítja meg a repó <link xlink:href="5_Mandelbrot/2mandelcomp.cpp">
                <filename>mandelcomp.cpp</filename>
            </link> nevű állománya.
        </para>
        <para>
        A programkód:
        </para>
        
        <programlisting language="c++"><![CDATA[
#include <iostream>
#include "png++/png.hpp"
#include <complex>

int
main ( int argc, char *argv[] )
{

  int szelesseg = 1920;
  int magassag = 1080;
  int iteraciosHatar = 255;
  double a = -1.9;
  double b = 0.7;
  double c = -1.3;
  double d = 1.3;

  if ( argc == 9 )
    {
      szelesseg = atoi ( argv[2] );
      magassag =  atoi ( argv[3] );
      iteraciosHatar =  atoi ( argv[4] );
      a = atof ( argv[5] );
      b = atof ( argv[6] );
      c = atof ( argv[7] );
      d = atof ( argv[8] );
    }
  else
    {
      if (argc==2)
      	std::cout << "Most az alapértékekkel dolgozunk." <<std::endl;
      else{    
      	std::cout << "Hasznalat: ./programnev fajlnev [szelesseg] [magassag] [n] [a] [b] [c] [d]" << std::endl;
	return -1;
	}
    }

  png::image < png::rgb_pixel > kep ( szelesseg, magassag );

  double dx = ( b - a ) / szelesseg;
  double dy = ( d - c ) / magassag;
  double reC, imC, reZ, imZ;
  int iteracio = 0;

  std::cout << "Szamitas\n";

  // j megy a sorokon
  for ( int j = 0; j < magassag; ++j )
    {
      // k megy az oszlopokon

      for ( int k = 0; k < szelesseg; ++k )
        {

          // c = (reC, imC) a halo racspontjainak
          // megfelelo komplex szam

          reC = a + k * dx;
          imC = d - j * dy;
          std::complex<double> c ( reC, imC );

          std::complex<double> z_n ( 0, 0 );
          iteracio = 0;

          while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
            {
              z_n = z_n * z_n + c;

              ++iteracio;
            }

          kep.set_pixel ( k, j,
                          png::rgb_pixel ( iteracio%255, (iteracio*iteracio)%255, 0 ) );
        }

      int szazalek = ( double ) j / ( double ) magassag * 100.0;
      std::cout << "\r" << szazalek << "%" << std::flush;
    }

  kep.write ( argv[1] );
  std::cout << "\r" << argv[1] << " mentve." << std::endl;

}
]]>
        </programlisting>  
        <para>
            Az előző feladatban bemutatott programmal ellentétben nem csak lebegőpontos számokkal történik a számolás, hanem a Mandelbrot halmaznak megfelelően már a teljes komplex számok halmazán.
            Illetve paraméterek beállításának lehetőségével is bővült ez a program:
        </para>
        <programlisting language="c++"><![CDATA[
int szelesseg = 1920;
int magassag = 1080;
int iteraciosHatar = 255;
double a = -1.9;
double b = 0.7;
double c = -1.3;
double d = 1.3;
        ]]>
        </programlisting>  
        <figure>
            <title>A Mandelbrot halmaz a komplex síkon</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/2/mandelcomp.png" scale="20" />
                </imageobject>
            </mediaobject>
        </figure>      
        <para>
            A program fordítása és futtatása:
        </para>
            <screen><![CDATA[
$ g++ mandelcomp.cpp -lpng
$ ../a.out mandelcomp.png
Most az alapértékekkel dolgozunk.
Szamitas
mandelcomp.png mentve.
]]></screen>     
    </section>        
                
    <section>
        <title>Biomorfok</title>
        <para>
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/anWem4Ts9ME">https://youtu.be/anWem4Ts9ME</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="5_Mandelbrot/3/biomorf.cpp"><filename>biomorf.cpp</filename></link>            
        </para>
        <para>
            A biomorfokra (a Julia halmazokat rajzoló bug-os programjával) 
            rátaláló Clifford Pickover azt hitte természeti törvényre 
            bukkant: <link xlink:href="https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf">https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf</link> (lásd a 2307. oldal aljától).
        </para>       
        <para>
            A különbség a <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmaz</link>
            és a Julia halmazok között az, hogy a komplex iterációban az előbbiben a c változó, utóbbiban pedig állandó. 
            A következő Mandelbrot csipet azt mutatja, hogy a c befutja a vizsgált összes rácspontot.
        </para>       
        <programlisting language="c++">
<![CDATA[  // j megy a sorokon
  for ( int j = 0; j < magassag; ++j )
    {
      for ( int k = 0; k < szelesseg; ++k )
        {

          // c = (reC, imC) a halo racspontjainak
          // megfelelo komplex szam

          reC = a + k * dx;
          imC = d - j * dy;
          std::complex<double> c ( reC, imC );

          std::complex<double> z_n ( 0, 0 );
          iteracio = 0;

          while ( std::abs ( z_n ) < 4 && iteracio < iteraciosHatar )
            {
              z_n = z_n * z_n + c;

              ++iteracio;
            }
]]>
        </programlisting>        
        <para>
            Ezzel szemben a Julia halmazos csipetben a cc nem változik, hanem minden vizsgált
            z rácspontra ugyanaz.
        </para>
        <programlisting language="c++">
<![CDATA[    // j megy a sorokon
    for ( int j = 0; j < magassag; ++j )
    {
        // k megy az oszlopokon
        for ( int k = 0; k < szelesseg; ++k )
        {
            double reZ = a + k * dx;
            double imZ = d - j * dy;
            std::complex<double> z_n ( reZ, imZ );

            int iteracio = 0;
            for (int i=0; i < iteraciosHatar; ++i)
            {
                z_n = std::pow(z_n, 3) + cc;
                if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                {
                    iteracio = i;
                    break;
                }
            }
]]>
        </programlisting>                         
        
        <para>
            A bimorfos algoritmus pontos megismeréséhez ezt a cikket javasoljuk: 
            <link xlink:href="https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf">https://www.emis.de/journals/TJNSA/includes/files/articles/Vol9_Iss5_2305--2315_Biomorphs_via_modified_iterations.pdf</link>.
            Az is jó gyakorlat, ha magából ebből a cikkből from scratch kódoljuk be a sajátunkat, de mi a királyi úton járva a 
            korábbi <link xlink:href="#bhax-textbook-feladatok-mandelbrot.Mandelbrot">Mandelbrot halmazt</link> kiszámoló 
            forrásunkat módosítjuk. Viszont a program változóinak elnevezését összhangba hozzuk a közlemény jelöléseivel:
        </para>       
        <programlisting language="c++">
<![CDATA[
#include <iostream>
#include "png++/png.hpp"
#include <complex>

int
main ( int argc, char *argv[] )
{

    int szelesseg = 1920;
    int magassag = 1080;
    int iteraciosHatar = 255;
    double xmin = -1.9;
    double xmax = 0.7;
    double ymin = -1.3;
    double ymax = 1.3;
    double reC = .285, imC = 0;
    double R = 10.0;

    if ( argc == 12 )
    {
        szelesseg = atoi ( argv[2] );
        magassag =  atoi ( argv[3] );
        iteraciosHatar =  atoi ( argv[4] );
        xmin = atof ( argv[5] );
        xmax = atof ( argv[6] );
        ymin = atof ( argv[7] );
        ymax = atof ( argv[8] );
        reC = atof ( argv[9] );
        imC = atof ( argv[10] );
        R = atof ( argv[11] );

    }
    else
    {
        std::cout << "Hasznalat: ./3.1.2 fajlnev szelesseg magassag n a b c d reC imC R" << std::endl;
        return -1;
    }

    png::image < png::rgb_pixel > kep ( szelesseg, magassag );

    double dx = ( xmax - xmin ) / szelesseg;
    double dy = ( ymax - ymin ) / magassag;

    std::complex<double> cc ( reC, imC );

    std::cout << "Szamitas\n";

    // j megy a sorokon
    for ( int y = 0; y < magassag; ++y )
    {
        // k megy az oszlopokon

        for ( int x = 0; x < szelesseg; ++x )
        {

            double reZ = xmin + x * dx;
            double imZ = ymax - y * dy;
            std::complex<double> z_n ( reZ, imZ );

            int iteracio = 0;
            for (int i=0; i < iteraciosHatar; ++i)
            {

                z_n = std::pow(z_n, 3) + cc;
                //z_n = std::pow(z_n, 2) + std::sin(z_n) + cc;
                if(std::real ( z_n ) > R || std::imag ( z_n ) > R)
                {
                    iteracio = i;
                    break;
                }
            }

            kep.set_pixel ( x, y,
                            png::rgb_pixel ( (iteracio*20)%255, (iteracio*40)%255, (iteracio*60)%255 ));
        }

        int szazalek = ( double ) y / ( double ) magassag * 100.0;
        std::cout << "\r" << szazalek << "%" << std::flush;
    }

    kep.write ( argv[1] );
    std::cout << "\r" << argv[1] << " mentve." << std::endl;

}
]]>
        </programlisting>                                 
        <para>
            A program fordítása és futtatása:
        </para>
        <screen><![CDATA[
$ g++ biomorf.cpp -lpng -o biomorf
$ ./biomorf biomorf.png 800 800 10 -2 2 -2 2 .285 0 10
Szamitas
biomorf.png mentve.
]]></screen>          
        <figure>
            <title>Biomorf</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/3/biomorf.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>                                     
                                                                            
    </section>                     

    <section>
        <title>A Mandelbrot halmaz CUDA megvalósítása</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/pMqn1Hwc16w">https://youtu.be/pMqn1Hwc16w</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="5_Mandelbrot/4/mandelcuda.cu"><filename>mandelcuda.cu</filename></link>       
        </para>
        <para>
            Az első Mandelbrot halmazzal foglalkozó programtól annyiban tér el ez a megközelítés, hogy itt a számítógép CUDA kártyájának segítségével fut le a program és rajzolódik ki a kép, 
            míg az első esetben a CPU dolgozott. A végeredmény megegyezik, viszont a CUDA futási ideje töredéke a CPU-nak, ami annak köszönhető, hogy a grafikus feladatok elvégzésében a videókártya 
            sokkal gyorsabban dolgozik, mint a processzor. Hiszen a CPU-s megoldás esetében szekvenciálisan dolgozik az adott program, a CUDA-s megvalósításban pedig a szálakon párhuzamosan számol.
        </para>
        <programlisting language="c++"><![CDATA[
#include "png++/image.hpp"
#include "png++/rgb_pixel.hpp"

#include <sys/times.h>
#include <iostream>


#define MERET 600
#define ITER_HAT 32000

__device__ int
mandel (int k, int j)
{
  // Végigzongorázza a CUDA a szélesség x magasság rácsot:
  // most eppen a j. sor k. oszlopaban vagyunk

  // számítás adatai
  float a = -2.0, b = .7, c = -1.35, d = 1.35;
  int szelesseg = MERET, magassag = MERET, iteraciosHatar = ITER_HAT;

  // a számítás
  float dx = (b - a) / szelesseg;
  float dy = (d - c) / magassag;
  float reC, imC, reZ, imZ, ujreZ, ujimZ;
  // Hány iterációt csináltunk?
  int iteracio = 0;

  // c = (reC, imC) a rács csomópontjainak
  // megfelelő komplex szám
  reC = a + k * dx;
  imC = d - j * dy;
  // z_0 = 0 = (reZ, imZ)
  reZ = 0.0;
  imZ = 0.0;
  iteracio = 0;
  // z_{n+1} = z_n * z_n + c iterációk
  // számítása, amíg |z_n| < 2 vagy még
  // nem értük el a 255 iterációt, ha
  // viszont elértük, akkor úgy vesszük,
  // hogy a kiinduláci c komplex számra
  // az iteráció konvergens, azaz a c a
  // Mandelbrot halmaz eleme
  while (reZ * reZ + imZ * imZ < 4 && iteracio < iteraciosHatar)
    {
      // z_{n+1} = z_n * z_n + c
      ujreZ = reZ * reZ - imZ * imZ + reC;
      ujimZ = 2 * reZ * imZ + imC;
      reZ = ujreZ;
      imZ = ujimZ;

      ++iteracio;

    }
  return iteracio;
}


/*
__global__ void
mandelkernel (int *kepadat)
{

  int j = blockIdx.x;
  int k = blockIdx.y;

  kepadat[j + k * MERET] = mandel (j, k);

}
*/

__global__ void
mandelkernel (int *kepadat)
{

  int tj = threadIdx.x;
  int tk = threadIdx.y;

  int j = blockIdx.x * 10 + tj;
  int k = blockIdx.y * 10 + tk;

  kepadat[j + k * MERET] = mandel (j, k);

}

void
cudamandel (int kepadat[MERET][MERET])
{

  int *device_kepadat;
  cudaMalloc ((void **) &device_kepadat, MERET * MERET * sizeof (int));

  // dim3 grid (MERET, MERET);
  // mandelkernel <<< grid, 1 >>> (device_kepadat);
  
  dim3 grid (MERET / 10, MERET / 10);
  dim3 tgrid (10, 10);
  mandelkernel <<< grid, tgrid >>> (device_kepadat);  
  
  cudaMemcpy (kepadat, device_kepadat,
	      MERET * MERET * sizeof (int), cudaMemcpyDeviceToHost);
  cudaFree (device_kepadat);

}

int
main (int argc, char *argv[])
{

  // Mérünk időt (PP 64)
  clock_t delta = clock ();
  // Mérünk időt (PP 66)
  struct tms tmsbuf1, tmsbuf2;
  times (&tmsbuf1);

  if (argc != 2)
    {
      std::cout << "Hasznalat: ./mandelpngc fajlnev";
      return -1;
    }

  int kepadat[MERET][MERET];

  cudamandel (kepadat);

  png::image < png::rgb_pixel > kep (MERET, MERET);

  for (int j = 0; j < MERET; ++j)
    {
      //sor = j;
      for (int k = 0; k < MERET; ++k)
	{
	  kep.set_pixel (k, j,
			 png::rgb_pixel (255 -
					 (255 * kepadat[j][k]) / ITER_HAT,
					 255 -
					 (255 * kepadat[j][k]) / ITER_HAT,
					 255 -
					 (255 * kepadat[j][k]) / ITER_HAT));
	}
    }
  kep.write (argv[1]);

  std::cout << argv[1] << " mentve" << std::endl;

  times (&tmsbuf2);
  std::cout << tmsbuf2.tms_utime - tmsbuf1.tms_utime
    + tmsbuf2.tms_stime - tmsbuf1.tms_stime << std::endl;

  delta = clock () - delta;
  std::cout << (float) delta / CLOCKS_PER_SEC << " sec" << std::endl;

}
        ]]>
        </programlisting>
        <para>
            A program fordítása (CUDA toolkit) és futtatása:
        </para>
        <screen><![CDATA[
$ nvcc mandelcuda.cu -o mandelcuda -lpng
$ ./mandelcuda mandelcuda.png
mandelcuda.png mentve
20
0.19959 sec
]]></screen>          
        <figure>
            <title>Mandelbrot halmaz</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/4/mandelcuda.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>
    </section>                     

    <section>
        <title>Mandelbrot nagyító és utazó C++ nyelven</title>
        <para>
            Építs GUI-t a Mandelbrot algoritmusra, lehessen egérrel nagyítani egy területet, illetve egy pontot
            egérrel kiválasztva vizualizálja onnan a komplex iteréció bejárta z<subscript>n</subscript> komplex számokat!
        </para>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/fSUCNOW7KEU">https://youtu.be/fSUCNOW7KEU</link>  
        </para>
        <para>
            Megoldás forrása:   <link xlink:href="5_Mandelbrot/5/frak.pro">frak.pro</link>, 
                                <link xlink:href="5_Mandelbrot/5/frakablak.cpp">frakablak.cpp</link>, 
                                <link xlink:href="5_Mandelbrot/5/frakablak.h">frakablak.h</link>, 
                                <link xlink:href="5_Mandelbrot/5/frakszal.cpp">frakszal.cpp</link>, 
                                <link xlink:href="5_Mandelbrot/5/frakszal.h">frakszal.h</link>, 
                                <link xlink:href="5_Mandelbrot/5/main.cpp">main.cpp</link>
        </para>
        <para>
            A program használatához telepíteni kell a Qt alkalmazás-keretrendszert. Ezt követően kiadható a <command>qmake</command> parancs, ami létrehozza a makefile-t. 
            Ha megvan a makefile, akkor kiadhatjuk a <command>make</command> parancsot, ami létrehozza a futtatható állományt, melyet a szokot módon futtathatunk.
        </para>
        <para>
            A lépések:
        </para>
        <screen><![CDATA[
$ qmake frak.pro
$ make
$ ./frank
]]></screen>  
        <para>
            A program az n gomb lenyomásával frissíti az iterációs határt, ezzel szebbé téve a képet.  
        </para>
        
        <figure>
            <title>Mandelbrot halmaz</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/5/1.png" scale="180" />
                </imageobject>
            </mediaobject>
        </figure>

        <figure>
            <title>Nagyítás 1X</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/5/2.png" scale="180" />
                </imageobject>
            </mediaobject>
        </figure>

        <figure>
            <title>Nagyítás 2X</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/5/3.png" scale="180" />
                </imageobject>
            </mediaobject>
        </figure>   
    </section>                     
                                                                                                                                                                            
    <section>
        <title>Mandelbrot nagyító és utazó Java nyelven</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/lQo13yubs-0">https://youtu.be/lQo13yubs-0</link>
        </para>
        <para>
            Megoldás forrása: <link xlink:href="5_Mandelbrot/6/MandelbrotHalmazNagyito.java">MandelbrotHalmazNagyito.java</link>
        </para>
        <para>
            Az előző feladathoz hasonlóan ez a program is bele tud nagyítani a Mandelbrot halmazba. Ahhoz, hogy le tudjuk futtatni, szükségünk van a 
            <link xlink:href="5_Mandelbrot/6/MandelbrotHalmaz.java">MandelbrotHalmaz.java</link> programra, ami az első feladat elvein alapulva kirajzolja a Mandelbrot halmazt. A nagyító 
            program meghívja a "rajzoló" programot, ezért egy könyvtárba kell őket helyezni. A fordítás-futtatás pedig a megszokott Java módszer:
        </para>
        <screen><![CDATA[
$ javac MandelbrotHalmazNagyito.java 
$ java MandelbrotHalmazNagyito 
]]></screen>  
    <para>
            A program az n gomb lenyomásával frissíti az iterációs határt, ezzel szebbé téve a képet, s gombbal pedig elmenti a képet.  
    </para>
        
        <figure>
            <title>Mandelbrot halmaz</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/6/1.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>

        <figure>
            <title>Nagyítás 1X</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/6/2.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>

        <figure>
            <title>Nagyítás 2X</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/6/3.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>   

        <figure>
            <title>Nagyítás 3X / szépítés</title>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="5_Mandelbrot/6/4.png" scale="50" />
                </imageobject>
            </mediaobject>
        </figure>   
    </section>
    <section>
        <title>Vörös Pipacs Pokol/fel a láváig és vissza</title>
        <para>
            Megoldás videó: <link xlink:href="https://youtu.be/f7lx7qMCj_I">https://youtu.be/f7lx7qMCj_I</link>      
        </para>
        <para>
            Megoldás forrása: <link xlink:href="5_Mandelbrot/7/rfh_fel-le.py">rfh_fel-le.py</link>               
        </para>
        <para>
            Az előző fejezetben lévő rfh feladathoz képest ezeket a változtatásokat kell eszközölni:
        </para> 
        <programlisting language="python"><![CDATA[
def run(self):
        world_state = self.agent_host.getWorldState()
        # Loop until mission ends:
        while world_state.is_mission_running:
            print("--- nb4tf4i arena -----------------------------------\n")
            if world_state.number_of_observations_since_last_state != 0:
                
                sensations = world_state.observations[-1].text
                print("    sensations: ", sensations)                
                observations = json.loads(sensations)
                nbr3x3x3 = observations.get("nbr3x3", 0)
                print("    3x3x3 neighborhood of Steve: ", nbr3x3x3)
                
                if "Yaw" in observations:
                    self.yaw = int(observations["Yaw"])
                if "Pitch" in observations:
                    self.pitch = int(observations["Pitch"])
                if "XPos" in observations:
                    self.x = int(observations["XPos"])
                if "ZPos" in observations:
                    self.z = int(observations["ZPos"])        
                if "YPos" in observations:
                    self.y = int(observations["YPos"])  
                
                print("    Steve's Coords: ", self.x, self.y, self.z)        
                print("    Steve's Yaw: ", self.yaw)        
                print("    Steve's Pitch: ", self.pitch)  
                
                if "LineOfSight" in observations:
                    lineOfSight = observations["LineOfSight"]
                    self.lookingat = lineOfSight["type"]
                print("    Steve's <): ", self.lookingat)  

                if "lava" in nbr3x3x3 or "flowing_lava" in nbr3x3x3:
                    self.agent_host.sendCommand( "turn -1" )
                    time.sleep(.1)
                    self.agent_host.sendCommand( "turn -1" )
                    time.sleep(.1)          
            
            self.agent_host.sendCommand( "move 1" )
            time.sleep(.3)   
            self.agent_host.sendCommand( "jumpmove 1" )
            time.sleep(.3)  
            world_state = self.agent_host.getWorldState()

    ]]> </programlisting>             
    </section>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
</chapter>                
