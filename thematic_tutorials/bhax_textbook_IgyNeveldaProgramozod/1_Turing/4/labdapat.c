#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak;
    ablak = initscr ();

    int x = 0; //sor
    int y = 0; //oszlop

    int xnov = 2; //x lépés távolság
    int ynov = 3; //y lépés távolság

    int mx; //ablak mérete (x,y)
    int my;

    for ( ;; ) {

        getmaxyx ( ablak, my , mx ); //ablak méretének megadása

        mvprintw ( y, x, "O" ); //a labda kiírása

        refresh ();
        usleep ( 100000 ); //a labda sebességének csökkentése
        //clear();
        x = x + xnov; 
        y = y + ynov;

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        if ( y<=0 ) { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        if ( y>=my-1 ) { // elerte-e a aljat?
            ynov = ynov * -1;
        }

    }

    return 0;
}
