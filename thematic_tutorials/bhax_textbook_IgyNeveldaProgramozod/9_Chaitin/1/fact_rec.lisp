#!/usr/bin/clisp

(defun fact_rec (n)
    (if (= n 0) 1
        (* n (fact_rec (- n 1)))))
    
    (loop for i from 0 to 20
        do (format t "~D! = ~D~%" i (fact_rec i)))
