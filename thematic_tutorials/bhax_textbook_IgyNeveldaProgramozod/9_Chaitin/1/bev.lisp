Üdvözli a TinyScheme
Copyright (c) Dimitrios Souflis
Script-Fu-konzol - Interaktív Scheme-fejlesztés

> (+ 2 2)
4
> (+ (+ 2 2) 2)
6
> (define (negyzet n) (* n n))
negyzet
> (negyzet 4)
16
> (negyzet (+ 3 2))
25
> (define (fakt n) (if (< n 1) 1 (* n (fakt ( - n 1)))))
fakt
> (fakt 3)
6
> (fakt 4)
24
> (fakt 5)
120
> (define (fakt n) (if (< n 2) 1 (* n (fakt ( - n 1)))))
fakt
> (fakt 4)
24
> (fakt 5)
120
> (define (fakt n) (if (< n 0) 1 (* n (fakt ( - n 1)))))
fakt
> (fakt 5)
0
