#!/usr/bin/clisp

(defun fact_ite (n)
    (let ((f 1))
        (dotimes (i n)
        (setf f (* f(+ i 1))))f)
)

(loop for i from 0 to 20
        do (format t "~D! = ~D~%" i (fact_ite i)))
