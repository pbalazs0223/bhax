#include <stdio.h>

int main()
{
    int *p;
    int (*ptr)[5];
    int array[5]={1,2,3,4,5};
    
    p = array;                      //a tömb első elemére mutat
    ptr = &array;                   //a tömb minden elemére mutat
    
    printf("%p, %p\n",p,ptr);
    ++p; ++ptr;                     //léptetjük a pointereket
    printf("%p, %p\n",p,ptr);
  
    
}
