#define MAX_TITKOS 4096
#define OLVASAS_BUFFER 256
#define KULCS_MERET 4 		// a kulcs méretének megadása
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <string.h>

double
atlagos_szohossz (const char *titkos, int titkos_meret)
{
    int sz = 0;
    for (int i = 0; i < titkos_meret; ++i)
        if (titkos[i] == ' ')
            ++sz;

    return (double) titkos_meret / sz;
}

int
tiszta_lehet (const char *titkos, int titkos_meret)
{
    // a tiszta szoveg valszeg tartalmazza a gyakori magyar szavakat
    // illetve az átlagos szóhossz vizsgálatával csökkentjük a
    // potenciális töréseket

    double szohossz = atlagos_szohossz (titkos, titkos_meret);

    return szohossz > 6.0 && szohossz < 9.0
           && strcasestr (titkos, "hogy") && strcasestr (titkos, "nem")
           && strcasestr (titkos, "az") && strcasestr (titkos, "ha");

}

void
exor (const char kulcs[], int kulcs_meret, char titkos[], int titkos_meret)
{

    int kulcs_index = 0;

    for (int i = 0; i < titkos_meret; ++i)
    {

        titkos[i] = titkos[i] ^ kulcs[kulcs_index];
        kulcs_index = (kulcs_index + 1) % kulcs_meret;

    }

}

int
exor_tores (const char kulcs[], int kulcs_meret, char titkos[],
            int titkos_meret)
{

    exor (kulcs, kulcs_meret, titkos, titkos_meret);

    return tiszta_lehet (titkos, titkos_meret);

}

int
main (void)
{

    char kulcs[KULCS_MERET];
    char titkos[MAX_TITKOS];
    char *p = titkos;
    int olvasott_bajtok;

    // titkos fájl beolvasása
    while ((olvasott_bajtok =
                read (0, (void *) p,
                      (p - titkos + OLVASAS_BUFFER <
                       MAX_TITKOS) ? OLVASAS_BUFFER : titkos + MAX_TITKOS - p)))
        p += olvasott_bajtok;

    // maradék hely nullazása a titkos bufferben
    for (int i = 0; i < MAX_TITKOS - (p - titkos); ++i)
        titkos[p - titkos + i] = '\0';

	// a kulcsot felépítő karakterek felsorolása
	char str[4] = {'a','k','k','o'}; 

    // összes kulcs előállítása; annyi ciklus, ahány karakteres a kulcs
    for (int ii = 0; ii <= 3; ++ii) 
	for (int li = 0; li <= 3; ++li)
	    for (int ki = 0; ki <= 3; ++ki)
	        for (int ji = 0; ji <= 3; ++ji)
		            {   //ahány karakteres a kulcs
                                    kulcs[0] = str[ii]; 
                                    kulcs[1] = str[li];
                                    kulcs[2] = str[ki];
                                    kulcs[3] = str[ji];
				    
                                    
                                    if (exor_tores (kulcs, KULCS_MERET, titkos, p - titkos))
                                        printf
					//ahány karakteres a kulcs
                                        ("Kulcs: [%c%c%c%c]\nTiszta szoveg: [%s]\n", 
					//ahány karakteres a kulcs
                                         kulcs[ii], kulcs[li], kulcs[ki], kulcs[ji], titkos);

                                    // újra EXOR-ozunk, igy nem kell egy második buffer
                                    exor (kulcs, KULCS_MERET, titkos, p - titkos);
                                }

    return 0;
}
