public class Fibonacci {

    public static void main(String[] args) {

        int max = 20;
        int prev = 0;
        int next = 1;

        System.out.println("The Fibonacci series is " +prev);
        while (next <= max) {
            if (next % 2 == 0) {
                System.out.println(next + " even");
            }
            else {
                System.out.println(next);
            }
            int sum = prev + next;
            prev = next;
            next = sum;
        }

    }
}
