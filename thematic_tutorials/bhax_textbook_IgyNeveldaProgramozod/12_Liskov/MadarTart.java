public class MadarTart {

    static class Madar {
    };

    static class RepuloMadar extends Madar {
        public void repul() {};
    };

    static class Sas extends RepuloMadar {

    }

    static class Pingvin extends Madar {

    }

    public static void main(String[] args) {
        Sas sas = new Sas();
        sas.repul();

        Pingvin pingvin = new Pingvin();
        //pingvin.repul(); //hibát ad

    }
}
